# Trigeering CI Pipline automatically 

Configure Webhook to trigfer CI Pipline automatically on every change 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Pipeline Job Steps](#pipeline-job-steps)

- [Multibranch Jobs Steps](#multibranch-jobs-steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Install GitLab Plugin in Jenkins 

* Configure GitLab access token and connection to Jenkins in GItlab project settings 

* Configure Jenkins to trigger the CI pipeline, whenever a change is pushed to GitLab 

## Technologies Used 

* Jenkins 

* Gitlab 

* Git 

* Docker 

* Java 

* Maven 

## Pipeline Job Steps 

Step 1: Install gitlab plugin in Jenkins 

[Gitlab Plugin](/images/01_install_gitlab_plugin_on_jenkins.png)
[Plugin Installed](/images/01_successfully_installed.png)

Step 2: Configure connection plugin in configure systems 

[Connection Plugin](/images/02_configuring_gitlab_connection_plugin.png)

Step 3: Create access token on gitlab 

[Gitlab Access Token](/images/03_creating_access_token_on_gitlab.png)

Step 4: Put gitlab access token credentials on Jenkins 

[Gitlab access token on Jenkins](/images/04_cofiguring_gitlab_access_token_crdentials_on_jenkins.png)
[Configured Gitlab Connection](/images/05_configured_gitlab_connections.png)

Step 5: Add configuration to Pipeline Job

[Configuration on Pipeline](/images/06_adding_new_configuration_on_mypipline.png)

Step 6: Configure gitlab to automatically send notification to jenkins when push occurs 

[Integration in Gitlab](/images/07_configuring_gitlab_automatically_send_notification_to_jenkins_when_push_happens.png)
[Active Setting](/images/08_active_setting.png)

Step 7: Test Automatic Triggering of Pipline Job

[Testing](/images/09_testing.png)
[Testing 1](/images/10_testing_1.png)
[Testing 2](/images/11_testing_2.png)

Step 8: Make changes to Jenkinsfile to test auto trigger 

[Changes to Jenkinsfile](/images/12_making_changes_to_jenkins_file_to_test.png)
[Pipeline Triggered](/images/13_pipline_automatically_triggered.png)

## Multibranch Jobs Steps

Step 1: Install Multibranch scan Webhook trigger plugin on Jenkins 

[Installing Webhook](/images/14_m_installing_multibranch_webhook_trigger.png)

Step 2: Configure Multibranch job by putting webhook plugin in configuration 

[Plugin in Configuration](/images/15_m_configuring_scan_multibranch_plugin.png)

Step 3: Get URL from Jenkins when inserting plugins as this will be needed for GitLab

[Getting URL](/images/16_getting_url_from_jenkins_for_gitlab_config.png)

Step 4: Go on Gitlab Webhooks and configure it to trigger on push events. You will do this by adding the URL in step 3 

[Webhooks GitLab](/images/17_configuring_gitlab_to_trigger_on_push_events.png)

Step 5: Edit Jenkinsfile to test auto triggering 

[Editing Jenkinsfile](/images/18_editing_jenkins_file_to_test.png)

Step 6: Go to Jenkins and check if it was auto triggered 

[Auto Triggered](/images/19_auto_triggered.png)

[Auto Triggered 2](/images/20_branch_that_was_changed_in_gitlab.png)

## Installation

Run $ apt install nodejs

## Usage 

Run $ java -jar java-maven-app-*.jar 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/trigger-ci-pipeline-automatically-on-every-change.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using mvn test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/trigger-ci-pipeline-automatically-on-every-change

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.